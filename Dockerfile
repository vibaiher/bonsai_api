FROM node:12.16.1-alpine3.11

ENV APP /opt/app
WORKDIR $APP

COPY package.json package-lock.json $APP/
RUN npm install

COPY . .

ENV PORT 3000
EXPOSE $PORT

CMD ["npm", "start"]
