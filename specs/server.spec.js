const request = require("supertest")
const app = require("../src/server")

describe("API", () => {
  it("responds 200 OK on /", async () => {
    const response = await request(app).get("/")

    expect(response.statusCode).toEqual(200)
    expect(response.text).toEqual("OK")
  })

  it("CORS is enabled for *", async () => {
    const response = await request(app).options("/")

    expect(response.statusCode).toEqual(204)
    expect(response.header["access-control-allow-origin"]).toEqual("*")
  })
})
