const Repository = require("../../src/collections/Resources")

describe("Repository", () => {
  it("fetchs resources", async () => {
    const resources = await Repository.fetchAll()

    expect(resources).toContainEqual({
      name: "Escuela Bonsai Valencia",
      categories: ["Escuelas"],
      url: "http://escuelabonsaivalencia.es",
    })
  })

  it("orders resources alphabetically", async () => {
    const resources = await Repository.fetchAll()

    expect(resources[0]).toEqual({
      name: "Alberto Gimeno, pinos thumbergii desde semilla",
      categories: ["Vídeos"],
      url: "https://youtu.be/FCRshqKQCNE",
    })
  })
})
