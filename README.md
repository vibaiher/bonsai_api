# Bonsai API

API to manage and categorise bonsai resources

## Development

### Requirements

- docker (version 19.03.8)
- docker-compose (version 1.25.3)
- A compatible linux terminal

### Useful commands

- `scripts/test` to run all tests once
- `scripts/test --watchAll` to run all tests and wait for changes (watch mode)
- `scripts/build` to build docker image and store it locally
- `scripts/start` to build and start a dev server on [localhost:3000](http://localhost:3000)

You can also use `docker-compose` command as usual.
